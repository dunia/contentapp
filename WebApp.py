#!/usr/bin/env python3

import socket

port = 1234

class webApp:

    def parse(self, peticion):
        print("Parse: Not parsing anything")
        return None

    def process(self, peticionparseada):
        print("Process: Returning 200 OK")
        return "200 OK", "<html><body><h1>It works!</h1></body></html>"

    def __init__ (self, hostname, port):
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))


        mySocket.listen(5)

        while True:
            print("Waiting for connections")
            (client_socket,  addr) = mySocket.accept()
            print("HTTP request received (going to parse and process):")
            peticion = client_socket.recv(2048)
            print(peticion)
            peticionparseada = self.parse(peticion.decode('utf8'))
            (returnCode, htmlAnswer) = self.process(peticionparseada)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            client_socket.send(response.encode('utf8'))
            client_socket.close()


if __name__ == "__main__":
    testWebApp = webApp("localhost", port)
