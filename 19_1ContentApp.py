#!/usr/bin/env python3

import WebApp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""


class ContentApp(WebApp.webApp):

    def __init__(self, hostname, port):
        self.contents = {'/': "<p>Main page</p>",
                         '/hello': "<p>Hello World</p>",
                         '/bye': "<p>Bye World!!</p>",

                         }
        super().__init__(hostname, port)

    def parse(self, peticion):

        return peticion.split(' ', 2)[1]

    def process(self, recurso):

        if recurso in self.contents:
            content = self.contents[recurso]
            pagina = PAGE.format(content=content)
            codigo = "200 OK"

        else:
            pagina = PAGE_NOT_FOUND.format(resource=recurso)
            codigo = "404 Resource Not Found"
        return codigo, pagina


if __name__ == "__main__":
    webApp = ContentApp("localhost", 1234)
